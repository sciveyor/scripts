require_relative "./evotext"

# Get the original JSON from Crossref for a given DOI.
def get_crossref_json(doi, log = DEFAULT_LOG)
  headers = { "Accept" => "application/vnd.citationstyles.csl+json" }

  url = "http://dx.doi.org/#{doi}"
  content = get_url_contents(url, 0.2, [204, 404], headers, log)
  return nil if content.nil?

  begin
    return JSON.parse(content)
  rescue JSON::ParserError
    # If this query fails, we just get an HTML page, which will fail to parse
    # and raise this error
    return nil
  end
end

# Extract a Crossref CSL date element
def get_crossref_date(element, progress)
  return nil unless element

  year = month = day = nil

  # Prefer using the 'date-parts' if we have them
  parts = element["date-parts"]
  if parts && parts.is_a?(Array)
    # These should be double-arrayed, but check in case they aren't
    parts = parts[0] if parts[0].is_a?(Array)

    year = parts[0]
    month = parts[1]
    day = parts[2]
  elsif element["raw"] && element["raw"].is_a?(String)
    # Try using the 'raw' if we have it
    parts = element["raw"].split("-")

    year = parts[0] if parts.size >= 1
    month = parts[1] if parts.size >= 2
    day = parts[2] if parts.size >= 3
  else
    progress.log(
      "ERROR: Asked to find a CSL date in #{
        element.inspect
      } but there's no date elements?"
    )
    return nil
  end

  year = year.to_s if year
  month = month.to_s if month
  day = day.to_s if day

  month = month.rjust(2, "0") if month
  day = day.rjust(2, "0") if day

  if !day.nil?
    if month.nil?
      progress.log(
        "   ...WARNING: date element has a day and year but no month?"
      )
      return year
    end

    return "#{year}-#{month}-#{day}"
  elsif !month.nil?
    return "#{year}-#{month}"
  elsif !year.nil?
    return year
  else
    # This shouldn't be possible?
    return nil
  end
end

# Get only the references part from a Crossref JSON blob, writing it into
# the requested file.
def crossref_write_references(crossref, path, log = DEFAULT_LOG)
  if crossref["reference"] && crossref["reference"].is_a?(Array) &&
       crossref["reference"].count > 0
    log.call("   ...found references, putting them in #{path}") if log

    path.open("w") do |of|
      of.write(JSON.pretty_generate(crossref["reference"]))
    end
  end
end

# Parse a Crossref file into a hash of canonical JSON schema parts. If you set
# dumpReferences to a Pathname object, we'll write out references there as JSON
# if we find them. If you set debug to true, you'll get detailed logging on
# stdout.
def parse_crossref(
  crossref,
  dataSource,
  dataSourceUrl,
  dataSourceVersion,
  progress = nil,
  dumpReferences = nil,
  debug = false
)
  # Start by parsing out all the dates
  date = get_crossref_date(crossref["published"], progress)
  date ||= get_crossref_date(crossref["published-print"], progress)
  date ||= get_crossref_date(crossref["published-online"], progress)
  date ||= get_crossref_date(crossref["issued"], progress)
  date ||= get_crossref_date(crossref["created"], progress)

  dateElectronic = get_crossref_date(crossref["published-online"], progress)

  if crossref["journal-issue"]
    date ||= get_crossref_date(crossref["journal-issue"]["published"], progress)
    date ||=
      get_crossref_date(crossref["journal-issue"]["published-print"], progress)
    date ||=
      get_crossref_date(crossref["journal-issue"]["published-online"], progress)

    dateElectronic ||=
      get_crossref_date(crossref["journal-issue"]["published-online"], progress)
  end

  dateElectronic = nil if date == dateElectronic
  progress.log("WARNING: no date available") if date.nil?

  # Build the article object
  ret = {}

  ret["schema"] = "https://data.sciveyor.com/schema"
  ret["version"] = 5

  ret["id"] = "doi:#{crossref["DOI"]}"
  ret["doi"] = crossref["DOI"]

  licenseYear = date&.split("-")&.first
  if crossref["license"] && crossref["license"][0]
    license = crossref["license"][0]
    ret["licenseUrl"] = license["URL"]

    if license["start"]
      licenseStartYear =
        get_crossref_date(license["start"], progress)&.split("-")&.first
      if licenseStartYear
        licenseYear = licenseStartYear
        progress.log("   ...found licenseYear in license") if debug
      end
    end
  end

  if crossref["publisher"]
    ret["license"] = "Copyright #{licenseYear} #{crossref["publisher"]}"
  else
    ret["license"] = "Copyright #{licenseYear} the publisher and/or authors"
  end

  ret["dataSource"] = dataSource
  ret["dataSourceUrl"] = dataSourceUrl
  ret["dataSourceVersion"] = dataSourceVersion

  ret["type"] = "article"

  # This is an extremely weird behavior, but account for it
  if crossref["title"].is_a?(Array) && crossref["title"].empty?
    crossref["title"] = nil
  end

  ret["title"] = super_strip(crossref["title"])

  if crossref["author"]
    ret["authors"] = []
    crossref["author"].each do |cra|
      au = {}

      # This follows the CSL spec for display order of names with form set to
      # "long", and collapsing all of the particles into the "middle" attribute
      # (which is not technically correct but is the best thing we've got).
      au["first"] = cra["given"] if cra["given"]

      if cra["dropping-particle"]
        au["middle"] = cra["dropping-particle"]
        if cra["non-dropping-particle"]
          au["middle"] += " #{cra["non-dropping-particle"]}"
        end
      elsif cra["non-dropping-particle"]
        au["middle"] = cra["non-dropping-particle"]
      end

      au["last"] = cra["family"] if cra["family"]
      au["suffix"] = cra["suffix"] if cra["suffix"]

      if cra["literal"]
        au["name"] = cra["literal"]
      else
        au["name"] = [
          au["first"],
          au["middle"],
          au["last"],
          au["suffix"]
        ].compact.join(" ")
      end

      if cra["affiliation"] && cra["affiliation"].count > 0
        au["affiliation"] = cra["affiliation"][0]

        # This is a very rare thing, but it does happen
        if au["affiliation"].is_a?(Hash)
          affname = au["affiliation"]["name"]
          if affname.nil?
            au["affiliation"] = nil
          else
            au["affiliation"] = affname
          end
        end
      end

      au["externalIds"] = ["orcid:#{cra["ORCID"]}"] if cra["ORCID"]

      ret["authors"] << au.compact
    end
  end

  ret["date"] = date if date
  ret["dateElectronic"] = dateElectronic if dateElectronic

  ret["journal"] = crossref["container-title"]
  ret["volume"] = crossref["volume"]
  ret["number"] = crossref["issue"]

  # FIXME: I feel like this might be a more complex object sometimes?
  unless crossref["page"].nil?
    if crossref["page"].is_a?(String)
      ret["pages"] = crossref["page"]
    else
      puts "   ...WARNING: Found a more complex object format for a page range!"
    end
  end

  ret["tags"] = crossref["subject"] if crossref["subject"] &&
    !crossref["subject"].empty?
  ret["abstract"] = super_strip(crossref["abstract"])

  crossref_write_references(crossref, dumpReferences) unless dumpReferences.nil?

  ret.compact
end
