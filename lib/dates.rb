require 'chronic'

# Merge two date fields, taking the one with more information. Pass a
# description of the element being parsed as desc to get informative error
# messages.
def merge_dates(a, b, desc = 'a date')
  # See which one has more fields, we'll return that if conflict
  a_count = a.compact.size
  b_count = b.compact.size

  if b_count > a_count
    better = b
  else
    better = a
  end

  # Check for conflict
  if a[0] && b[0] && a[0] != b[0]
    puts "WARNING: years do not match for #{desc}, taking better"
    return better
  end

  if a[1] && b[1] && a[1] != b[1]
    puts "WARNING: months do not match for #{desc}, taking better"
    return better
  end

  if a[2] && b[2] && a[2] != b[2]
    puts "WARNING: days do not match for #{desc}, taking better"
    return better
  end

  [a[0] || b[0], a[1] || b[1], a[2] || b[2]]
end

# Parse a string date using both Date.parse and Chronic.parse, trying to get
# whatever kind of content out of it we can. Pass a description of this element
# as desc to get informative error messages.
def parse_string_date(str, desc = 'a date')
  d_date = Date.parse(str)
  c_date = Chronic.parse(str)

  if d_date.nil? && c_date.nil?
    # Okay, give up entirely
    return nil, nil, nil
  end

  if d_date
    a = [d_date.year, d_date.month, d_date.day]
  else
    a = [nil, nil, nil]
  end

  if c_date
    b = [c_date.year, c_date.month, c_date.day]
  else
    b = [nil, nil, nil]
  end

  merge_dates(a, b, desc)
end

# Clean up a date, leaving behind only the parts that it actually contains, and
# printing an error message if we have a year and day but no month.
def clean_date(date)
  return nil if date.compact.empty?

  if date[0] && date[1].nil? && date[2]
    puts 'WARNING: date with nil month but present day?'
    return [date[0]]
  end

  date.compact
end

# Convert a date to string, making sure that we add leading zeros if needed.
def date_to_string(date)
  date = clean_date(date)

  return nil if date.nil?

  date[0] = date[0].to_s
  date[1] = date[1].to_s.rjust(2, '0') if date.count > 1
  date[2] = date[2].to_s.rjust(2, '0') if date.count > 2

  date.join('-')
end
