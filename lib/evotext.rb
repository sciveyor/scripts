# Utility functions for all of the evoText data maintenance scripts
require 'fileutils'
require 'open-uri'
require 'timeout'
require 'erb'
require 'loofah'

# Return the path to the .env file. It's easiest to do this once here, because
# this file is loaded by everyone and its path doesn't change.
def dotenv_path
  File.join(File.expand_path(File.dirname(__FILE__)), '..', '.env')
end

# A default logging proc, used as a function default parameter
DEFAULT_LOG = Proc.new { |s| puts(s) }

# Return the list of files to be worked on in an array.  This supports the user
# passing in any number of files or directories as arguments to the function.
#
# With a block, yield each filename (more efficient, because it doesn't require
# reading the filename array into memory).  Without a block, return the files
# in an array.
#
# glob_for_args('.xml') do |filename|
#   (process filename)
# end
#
# If you want to get all files, don't pass an extension at all.
def glob_for_args(extension = nil)
  # Extension needs to start with a dot if provided
  extension = '.' + extension unless extension.start_with?('.') if extension

  ret = [] unless block_given?

  ARGV.each do |arg|
    abort "ERROR: Specified file #{arg} does not exist" unless File.exist?(arg)

    if File.directory? arg
      if extension
        pattern = "#{arg}/**/*#{extension}"
      else
        pattern = "#{arg}/**/*"
      end

      if block_given?
        Dir.glob(pattern) { |filename| yield filename }
      else
        ret += Dir.glob(pattern)
      end
    else
      if extension
        if File.extname(arg) != extension
          abort "ERROR: Specified file #{arg} does not have extension #{
                  extension
                }"
        end
      end

      if block_given?
        yield arg
      else
        ret << arg
      end
    end
  end

  block_given? ? nil : ret
end

# Perform a safe move, throwing an exception if destination already exists.
#
# Also handles 'src' as an array.
def safe_mv(src, dest, ignore_existing = false)
  src = [src] unless src.is_a?(Array)

  src.each do |f|
    basename = File.basename(f)
    destpath = Pathname.new(dest) + basename

    if destpath.exist?
      if ignore_existing
        return
      else
        abort "ERROR: Trying to move #{f} to #{
                dest
              }, but destination already exists!"
      end
    end

    FileUtils.mv(f, dest)
  end
end

# Clean up a DOI to make sure that it's a valid URL.
#
# Some ancient DOIs have unencoded, non-URL-friendly characters in them. We try
# here to salvage those into something we could use to query URIs with.
def clean_doi_url(url)
  # Split this into beginning-part and end-part
  match = %r{https?://(dx\.)?doi\.org/([0-9.]+)/(.*)}.match(url)

  return url if match.nil?

  # URL-encode the last bit
  return "https://doi.org/#{match[2]}/#{ERB::Util.url_encode(match[3])}"
end

# Fetch the string at the given URL, retrying up to 10 times and gracefully
# handling errors.
#
# You can customize how long you'd like to sleep after a successful fetch; it
# will also sleep for longer than this when it attempts to wait out and retry
# around various kinds of error conditions. It also sets internal timeouts for
# URI's HTTP functions, will retry if they're hit, and fail after too many
# retries.
#
# It will print error messages to the console if you don't set quiet to true.
#
# If you would like to treat certain HTTP errors as fatal (i.e., to accept
# failure without retrying if we receive certain HTTP error codes), you can
# pass those as an array of integers. If they're encountered when we try to
# access the page, we'll print a warning and return rather than retrying.
#
# If you add more headers in "extra_headers", we'll merge them into the
# fetch call.
def get_url_contents(
  url,
  sleep_time = 0.33,
  fatal_errors = [],
  extra_headers = {},
  log = DEFAULT_LOG
)
  # Clean the URL
  url = clean_doi_url(url)

  options = {
    # Add a normal desktop-looking User-Agent, some sites seem to balk if you
    # don't.
    'User-Agent' =>
      'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/69.0',
    # Add some connection options (symbol keys)
    :read_timeout => 10,
    :open_timeout => 10
  }
  options.merge!(extra_headers) unless extra_headers.empty?

  retries ||= 0
  content = URI.open(url, options).read

  sleep(sleep_time)
  return content
rescue Timeout::Error
  retry if (retries += 1) < 10
rescue OpenURI::HTTPError => error
  response = error.io
  status = response.status[0]

  # See if we shouldn't even bother retrying
  if fatal_errors.include?(status.to_i)
    if log
      log.call(
        "ERROR: Received a fatal error code (#{status}) when accessing #{
          url
        }, not retrying..."
      )
    end
    sleep(sleep_time)
    return nil
  end

  # Give it a few tries in case it's just network congestion
  sleep(3 * sleep_time)
  retry if (retries += 1) < 10

  # Fail hard
  log.call("ERROR: Unknown network error fetching #{url} (#{status})...") if log
  sleep(sleep_time)
  return nil
rescue RuntimeError => error
  # There's this one weird message that occasionally occurs when we try to
  # get pages, and just waiting it out seems to fix it.
  if error.message.start_with?('HTTP redirection loop:')
    sleep(10 * sleep_time)
    retry if (retries += 1) < 10
  end

  # Fail hard
  if log
    log.call(
      "ERROR: Unknown network error fetching #{url} (#{error.message})..."
    )
  end
  sleep(sleep_time)
  return nil
end

# Industrial strength HTML tag stripping.
def super_strip(text)
  return nil if text.nil?

  frag = Loofah.fragment(text)
  frag.to_text.strip.gsub(/\s+/, ' ')
end

# Print out some errors and warnings if important fields in the canonical
# schema are missing.
def lint_canonical_schema(doc)
  error_fields = %w[
    schema
    version
    id
    doi
    license
    licenseUrl
    dataSource
    dataSourceUrl
    dataSourceVersion
    title
    journal
    year
    volume
    number
    fullText
  ]
  warning_fields = %w[authors abstract pages]

  error_fields.each do |f|
    next if doc[f]
    puts "   ...ERROR: no value found for #{f}"
  end

  warning_fields.each do |f|
    next if doc[f]
    puts "   ...WARNING: no value found for #{f}"
  end
end
