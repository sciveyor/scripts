require_relative "./evotext"
require_relative "./dates"

require "dotenv"
Dotenv.load(dotenv_path)

require "nokogiri"

# Parse the text-formatted month in PubMed date entries, returning an integer.
def parse_pubmed_month(month)
  return nil if month.nil?

  names = {
    "Jan" => 1,
    "Feb" => 2,
    "Mar" => 3,
    "Apr" => 4,
    "May" => 5,
    "Jun" => 6,
    "Jul" => 7,
    "Aug" => 8,
    "Sep" => 9,
    "Oct" => 10,
    "Nov" => 11,
    "Dec" => 12
  }

  return names[month] if names.include?(month)

  # Throws ArgumentError!
  Integer(month)
end

# Extract a PubMed-formatted date from an XML element containing the year, month
# and date fields (in any of the wide variety of ways that PubMed might give
# them to us). Returns an array with [year, month, day], in which any and all
# of those elements might be nil.
def get_pubmed_date(element)
  return nil, nil, nil unless element

  # See if we have MedlineDate instead of Year/Month/Day
  if element.at_css("Year").nil?
    mld = element.at_css("MedlineDate")
    if mld
      return parse_string_date(mld.text, "pubmed mld parsing of #{mld.text}")
    end

    # Weird?
    puts "ERROR: Asked to find a date in #{
           element.inspect
         } but there's no date elements?"
    return nil, nil, nil
  end

  year = nil
  begin
    year = Integer(element.at_css("Year")&.text)
  rescue ArgumentError, TypeError
    year = nil
  end

  month = nil
  begin
    month = parse_pubmed_month(element.at_css("Month")&.text)
  rescue ArgumentError, TypeError
    month = nil
  end

  day = nil
  begin
    day = Integer(element.at_css("Day")&.text)
  rescue ArgumentError, TypeError
    day = nil
  end

  [year, month, day]
end

# Return the PubMed record for the given DOI, if it exists, else nil
def get_pubmed_xml(doi: nil, pmcid: nil, log: DEFAULT_LOG)
  # Make sure we have an identifier
  if doi.nil? && pmcid.nil?
    log.call("ERROR: Neither doi nor pmcid in get_pubmed_xml") if log
    return nil
  end

  id =
    if doi
      doi
    elsif pmcid
      pmcid
    end

  # Three requests per second are required as an upper limit by the API, unless
  # you pass an API key in dotenv.
  sleep_time = 0.33
  apikey = nil

  unless ENV["PUBMED_API_KEY"]
    if log
      log.call(
        "WARNING: Not using an API key, you should consider putting one in .env"
      )
    end
  else
    apikey = ENV["PUBMED_API_KEY"]
    sleep_time = 0.1
  end

  # Run through idconv and get the PMID
  idconv_url = "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?"
  idconv_url += "tool=sciveyor"
  idconv_url += "&email=charles@charlespence.net"
  idconv_url += "&ids=#{id}"

  content = get_url_contents(idconv_url, sleep_time, [], {}, log)
  return nil if content.nil?

  # Parse out the PMID, if it's there
  doc = Nokogiri.XML(content)
  if doc.root.name != "pmcids"
    log.call("ERROR: idconv document isn't an pmcids for #{id}!") if log
    return nil
  end

  record = doc.at_css("pmcids record")
  if record.nil?
    log.call("WARNING: pmcids document doesn't have record for #{id}!") if log
    return nil
  end

  pmid = record["pmid"]
  if pmid.nil?
    # This is the expected behavior if the document is not found in PubMed, so
    # don't tell me about it.
    #
    # log.call("WARNING: pcids document has a hit but no pmid for #{id}!") if log
    return nil
  end

  pmid.strip!

  log.call("...fetching #{pmid} for #{id}...") if log

  efetch_url =
    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed"
  efetch_url += "&api_key=#{apikey}" if apikey

  # Run it through esummary and get everything they know about
  url = efetch_url + "&id=#{pmid}&retmode=xml"
  get_url_contents(url, sleep_time, [], {}, log)
end

# Add external IDs to the given document object, taken from the PubMed XML
def add_pubmed_externalids(output, pubmed, debug = false, log = DEFAULT_LOG)
  return if pubmed.nil?
  pubmed = Nokogiri.XML(pubmed) if pubmed.is_a?(String)

  pubmed
    .css("PubmedData > ArticleIdList ArticleId")
    .each do |aid|
      content = aid.text.strip

      case aid["IdType"]
      when "pubmed"
        newid = "pmid:#{content}"
      when "pii"
        newid = "pii:#{content}"
      when "pmc"
        newid = "pmcid:#{content}"
      when "mid"
        newid = "pmmid:#{content}"
      when "doi"
        next
      else
        if log
          log.call(
            "   ...WARNING: Found PubMed Article ID of type #{
              aid["IdType"]
            }, don't know what to do with it?"
          )
        end
      end

      # Add it to the list if it's not already there
      output["externalIds"] ||= []
      next if output["externalIds"].include?(newid)
      output["externalIds"] << newid

      if debug && log
        log.call("   ...added ID of type #{aid["IdType"]} from PubMed")
      end
    end
end

# Add author affiliations to the given document object, taken from the
# PubMed XML
def add_pubmed_affiliations(output, pubmed, log = DEFAULT_LOG)
  # Make sure that the data isn't nil, and has been parsed
  return if pubmed.nil?
  pubmed = Nokogiri.XML(pubmed) if pubmed.is_a?(String)

  # Author affiliations
  if output["authors"]
    pubmed
      .css("AuthorList Author")
      .each_with_index do |pau, idx|
        if output["authors"].count <= idx
          if log
            log.call("   ...WARNING: expected not to have this many authors")
          end
          break
        end

        next if output["authors"][idx]["affiliation"]

        pau_ai = pau.at_css("AffiliationInfo")
        next unless pau_ai

        pau_aff = pau_ai.at_css("Affiliation")
        next unless pau_aff

        pau_affiliation = pau_aff.text
        next unless pau_affiliation && !pau_affiliation.empty?

        output["authors"][idx]["affiliation"] = pau_affiliation
      end
  end
end

# Add the abstract from PubMed to this record, if it's better than what we
# already have
def add_pubmed_abstract(output, pubmed, debug = false, log = DEFAULT_LOG)
  return if pubmed.nil?
  pubmed = Nokogiri.XML(pubmed) if pubmed.is_a?(String)

  json_abstract = output["abstract"]
  pubmed_abstract_elt = pubmed.at_css("Article Abstract AbstractText")
  pubmed_abstract = pubmed_abstract_elt ? pubmed_abstract_elt.text : ""

  if json_abstract == pubmed_abstract
    log.call("   ...abstracts are identical") if log && debug
    abstract = pubmed_abstract
  elsif json_abstract.empty? && !pubmed_abstract.empty?
    log.call("   ...have PubMed abstract but no JSON abstract") if log && debug
    abstract = pubmed_abstract
  elsif pubmed_abstract.empty? && !json_abstract.empty?
    log.call("   ...have JSON abstract but no PubMed abstract") if log && debug
    abstract = json_abstract
  else
    lengthDiff = (json_abstract.length - pubmed_abstract.length).abs
    if lengthDiff < 20
      log.call("   ...close in length, preferring PubMed") if log && debug
      abstract = pubmed_abstract
    else
      if log && debug
        log.call("   ...very different in length, preferring the longer one")
      end

      if json_abstract.length > pubmed_abstract.length
        abstract = json_abstract
      else
        abstract = pubmed_abstract
      end
    end
  end

  output["abstract"] = abstract
end

# Dump the references from the PubMed XML to a file.
def dump_pubmed_references(pubmed, reference_file, log = DEFAULT_LOG)
  # Make sure that the data isn't nil, and has been parsed
  return if pubmed.nil?
  pubmed = Nokogiri.XML(pubmed) if pubmed.is_a?(String)

  # References
  refs = pubmed.css("ReferenceList Reference")
  unless refs.nil? || refs.empty?
    ret = []
    refs.each do |r|
      ref = {}

      cite = r.at_css("Citation")
      ref["Citation"] = cite.text.strip if cite

      aids = r.css("ArticleIdList ArticleId")
      if aids
        ref["ArticleIDs"] = {}
        aids.each { |aid| ref["ArticleIDs"][aid["IdType"]] = aid.text.strip }
      end

      ret << ref if ref.count
    end

    log.call("   ...found references, putting them in #{reference_file}") if log
    reference_file.open("w") { |of| of.write(JSON.pretty_generate(ret)) }
  end
end
