#!/usr/bin/env ruby
# For each DOI file that we're passed, read it, go get the abstract from the
# nature.com HTML page for it

$:.unshift File.join(File.expand_path(File.dirname(__FILE__)),
                     '..', '..', 'lib')
require 'evotext'
require 'fileutils'
require 'pathname'
require 'nokogiri'

base_url = 'https://www.nature.com/articles/'
sleep_time = 0

glob_for_args('.doi') do |doifile|
  doi = File.read(doifile).strip

  outfilename = Pathname(doifile).sub_ext('.abstract.txt')
  if outfilename.exist?
    puts "output abstract file #{outfilename} already exists, skipping..."
    next
  end

  url = base_url + doi.sub('10.1038/', '')
  puts "fetching #{url}..."

  content = get_url_contents(url, sleep_time)
  next if content.nil?

  # Parse out the abstract, if there is one
  doc = Nokogiri::HTML(content)

  meta_desc = doc.at_css("meta[name='dc.description']")
  if meta_desc.nil?
    puts "WARNING: Should at least have an empty meta tag for description, nothing for #{doi}!"
    FileUtils.touch(outfilename)
    next
  end

  # This could very well be empty, don't treat that as an error!
  abstract = meta_desc['content']

  outfilename.open('w') do |of|
    of.write(abstract)
  end
  puts "   ...wrote #{outfilename}"
end
