#!/usr/bin/env ruby
# Get PDFs from an OJS journal with DOIs.

$:.unshift File.join(File.expand_path(File.dirname(__FILE__)), "..", "lib")
require "evotext"
require "optparse"
require "httparty"
require "nokogiri"
require "open-uri"

# Parse options immediately
options = { error_log: "failed" }
OptionParser
  .new do |opts|
    opts.banner = "Usage: ojs_pdfs --input <doi-list> [options]"

    opts.on("-h", "--help", "Prints this help") do
      puts opts
      exit
    end
    opts.on(
      "-i",
      "--input STRING",
      "The file containing our list of DOIs to fetch"
    ) { |v| options[:input] = v }
    opts.on(
      "-e",
      "--error-log STRING",
      "The file to which we'll write failures (default: failed)"
    ) { |v| options[:error_log] = v }
  end
  .parse!

unless options[:input]
  raise OptionParser::MissingArgument, "You must specify a value for --input"
end

if File.exist?(options[:error_log])
  puts "ERROR: Refusing to overwrite error log at #{options[:error_log]}, remove it and try again"
  exit
end

File
  .readlines(options[:input])
  .each do |doi|
    doi.strip!
    clean_doi = doi.gsub(%r{[./]}, "_")

    if File.exist?("#{clean_doi}.pdf")
      puts "...#{doi} already exists, skipping"
      next
    end

    response = HTTParty.get("https://doi.org/#{doi}")
    if response.code != 200
      puts "ERROR: Failure in getting DOI page for #{doi} (#{response.code})"
      File.open(options[:error_log], "a") { |f| f.puts(doi) }
      next
    end

    # Parse the response
    doc = Nokogiri.HTML(response.body)
    meta = doc.at_css('meta[name="citation_pdf_url"]')
    if meta.nil?
      puts "ERROR: Cannot find <meta name='citation_pdf_url'> in document for #{doi}"
      File.open(options[:error_log], "a") { |f| f.puts(doi) }
      next
    end

    pdf_url = meta[:citation_pdf_url]
    if pdf_url.nil? || pdf_url.empty?
      puts "ERROR: Empty PDF URL attribute in #{doi}"
      File.open(options[:error_log], "a") { |f| f.puts(doi) }
      next
    end

    # Get the PDF
    download = open(pdf_url)
    IO.copy_stream(download, "#{clean_doi}.pdf")
    File.open("#{clean_doi}.doi", "w") { |f| f.write(doi) }

    # Sleep for a bit to be safe
    sleep 3
  end
