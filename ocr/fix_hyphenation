#!/usr/bin/env ruby
# Fix the hyphenation of a file that comes from Tesseract.
#
# Tesseract, correctly handles multi-column documents, but it leaves most
# hyphenated words broken, with the hyphen at the end of the line. This script
# loads the text file that it is passed, and modifies it in-place as follows:
#
# 1) If a line ends with one of the hyphen characters defined below, then:
#    a) check to see whether the dashed word on that line, the first word on
#       the next line, and the two words merged, removing a dash, appear in a
#       spelling dictionary
#    b) if the merged word appears in the dictionary, replace with that and
#       stop
#    c) if both the unmerged words appear, and the merged word does not, then
#       replace it with a hyphenated single word (e.g., first- and time will
#       become 'first-time')
#    d) otherwise, merge the words together, removing the dash

require 'pathname'
require 'ffi/aspell'

# Detect both standard ASCII dash and Unicode hyphen and non-breaking hyphen
HYPHENS = '-‐‑'

# FIXME: Add language choice
speller = FFI::Aspell::Speller.new('en')

# Command-line arguments
if ARGV.size != 1
  fail 'ERROR: Must provide the path to the text file you wish to clean'
end

# Read the file
filename = Pathname.new(ARGV[0])
fail "ERROR: File #{in_file} does not exist" unless filename.exist?

lines = nil
filename.open('r') { |in_file| lines = in_file.readlines }

lines.each_with_index do |line, i|
  break if i == lines.count - 1
  next_line = lines[i + 1]

  # Look for a line that ends with a hyphen
  next if line.length == 1
  last = line[-2]

  next unless HYPHENS.include?(last)

  # Okay, we have one; get the two partial-word bits
  match = /(\p{Word}+)[#{HYPHENS}]+\Z/.match(line)
  next if match.nil? # Should be impossible

  line_word = match[1]
  next if line_word.nil? # Should be impossible

  match = /\A[\p{Word}\p{Punct}]+/.match(next_line)
  next if match.nil? # Rare but might happen

  next_line_word = match[0]
  next if next_line_word.nil? # Should be impossible

  # And build the merged word
  merged_word = line_word + next_line_word

  # First test: if merged_word is correct, replace
  if speller.correct?(merged_word)
    merge = true
  else
    # Second test: if both unmerged words appear, hyphenate
    if speller.correct?(line_word) && speller.correct?(next_line_word)
      merge = false
    else
      # Merge anyway, assume technical term or OCR error
      merge = true
    end
  end

  # Next line is always the same (strip first word)
  new_next_line = next_line.gsub(/\A[\p{Word}\p{Punct}]+\p{Blank}*/, '')

  if merge
    # Replace the partial hyphenated word with merged
    new_line = line.gsub(/\p{Word}+[#{HYPHENS}]+\Z/, merged_word)

    lines[i] = new_line
    lines[i + 1] = new_next_line
  else
    # Replace the partial hyphenated word with hyphenated
    new_line =
      line.gsub(/\p{Word}+[#{HYPHENS}]+\Z/, "#{line_word}-#{next_line_word}")

    lines[i] = new_line
    lines[i + 1] = new_next_line
  end
end

# Write the file back
filename.open('w') { |out_file| lines.each { |line| out_file.write(line) } }
