#!/usr/bin/env ruby
# Parse the crossref metadata, plus text files, plus PubMed information, into
# an initial form of our internal JSON file.
$:.unshift File.join(File.expand_path(File.dirname(__FILE__)), "..", "lib")
require "evotext"
require "crossref"
require "pubmed"
require "pathname"
require "json"
require "ruby-progressbar"
require "optparse"

# Parse options immediately
options = {}
OptionParser
  .new do |opts|
    opts.on(
      "-s",
      "--data-source STRING",
      "The dataSource attribute for the generated JSON files"
    ) { |v| options[:data_source] = v }
    opts.on(
      "-u",
      "--data-source-url URL",
      "The dataSourceUrl attribute for the generated JSON files"
    ) { |v| options[:data_source_url] = v }
    opts.on(
      "-v",
      "--data-source-version NUM",
      "The dataSourceVersion attribute for the generated JSON files"
    ) { |v| options[:data_source_version] = Integer(v, 10) }
    opts.on(
      "-l",
      "--default-license STRING",
      "Use this string as license if none is in a given article file"
    ) { |v| options[:license_default] = v }
    opts.on(
      "-i",
      "--default-license-url URL",
      "Use this URL for licenseUrl if none is in a given article file"
    ) { |v| options[:license_url_default] = v }
    opts.on(
      "-f",
      "--log-file FILE",
      "Log any information to a log file in addition to stdout"
    ) { |v| options[:log_file] = v }
    opts.on(
      "-d",
      "--debug",
      "Logs copious amounts of information to stdout/log during parsing"
    ) { |v| options[:debug] = true }
  end
  .parse!

unless options[:data_source]
  raise OptionParser::MissingArgument,
        "You must specify a value for --data-source"
end
unless options[:data_source_url]
  raise OptionParser::MissingArgument,
        "You must specify a value for --data-source-url"
end
unless options[:data_source_version]
  raise OptionParser::MissingArgument,
        "You must specify a value for --data-source-version"
end

if options[:log_file]
  # This is a code smell, but we have to get the filename into the method
  # somehow or other
  $global_log_filename = options[:log_file]

  module ProgBarExtensions
    def log(string)
      File.open($global_log_filename, "a+") { |f| f.puts(string) }
      super
    end
  end

  class ProgressBar::Base
    prepend ProgBarExtensions
  end
end

crossref_list = glob_for_args(".crossref")
progress =
  ProgressBar.create(
    format: "%a [%B] %e %j%% %t",
    title: "Parsing .crossref files",
    total: crossref_list.size
  )

crossref_list.each do |crossref_filename|
  progress.increment
  progress.log("parsing #{crossref_filename}")

  outfile = Pathname(crossref_filename).sub_ext(".json")
  next if outfile.exist?

  ret = {}

  # Get the Crossref data
  crossref_data = JSON.parse(File.read(crossref_filename))
  out_cr_references = outfile.sub_ext(".cr-refs-json")
  ret =
    parse_crossref(
      crossref_data,
      options[:data_source],
      options[:data_source_url],
      options[:data_source_version],
      progress,
      out_cr_references,
      options[:debug]
    )

  # Set the default licenses
  if ret["license"].nil? || ret["license"].empty?
    if options[:license_default]
      ret["license"] = options[:license_default]
    else
      progress.log "   ...FATAL ERROR: license default not set, none found"
      exit
    end
  end

  if ret["licenseUrl"].nil? || ret["licenseUrl"].empty?
    if options[:license_url_default]
      ret["licenseUrl"] = options[:license_url_default]
    else
      progress.log "   ...FATAL ERROR: license url default not set, none found"
      exit
    end
  end

  # Get the full text
  fulltext_filename = Pathname(crossref_filename).sub_ext(".txt")
  fullText = File.read(fulltext_filename)
  ret["fullText"] = fullText

  # Get the PubMed data and overlay it, if we have a DOI to search by
  out_pm_references = outfile.sub_ext(".pm-refs-json")
  if ret["doi"]
    pubmed_xml =
      get_pubmed_xml(
        doi: ret["doi"],
        log: ->(s) { progress.log(s) if options[:debug] }
      )

    add_pubmed_externalids(
      ret,
      pubmed_xml,
      options[:debug],
      ->(s) { progress.log(s) if options[:debug] }
    )
    add_pubmed_affiliations(
      ret,
      pubmed_xml,
      ->(s) { progress.log(s) if options[:debug] }
    )
    dump_pubmed_references(
      pubmed_xml,
      out_pm_references,
      ->(s) { progress.log(s) if options[:debug] }
    )
  end

  # Write out the final output
  outfile.open("w") { |of| of.write(JSON.pretty_generate(ret)) }
end
