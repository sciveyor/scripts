#!/usr/bin/env ruby
# Change all files under the given path with one extension to have a different
# extension.
#
# For instance, calling
#
# change_extension .json .backup
#
# will rename all files like "asdf.json" to "asdf.backup".

$:.unshift File.join(File.expand_path(File.dirname(__FILE__)), '..', 'lib')
require 'evotext'
require 'fileutils'
require 'pathname'

if ARGV.size < 3
  puts 'Usage: change_extension <.oldex> <.newex> <paths>'
  exit 1
end

old_extension = ARGV.shift
new_extension = ARGV.shift

old_extension = '.' + old_extension unless old_extension.start_with?('.')
new_extension = '.' + new_extension unless new_extension.start_with?('.')

glob_for_args(old_extension) do |input|
  old_path = Pathname.new(input)
  new_path = old_path.sub_ext(new_extension)

  if new_path.exist?
    puts "ERROR: Output file #{new_path} already exists; skipping"
    next
  end

  FileUtils.mv(old_path, new_path)
end
