Some of the content in this directory is not released under CC0 like
the other files in this script archive.  Modified or original
versions of the following software are present here, including the
following licensing terms:


## W3C DTD Files

Several DTD files from the W3C, including the DTD for MathML 2, are
present here.  This code is distributed under the W3C software
license.  The full terms of that license are available at:

http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231

and their short notice states that these files are:

Copyright © 1998-2003 World Wide Web Consortium (Massachusetts
Institute of Technology, European Research Consortium for Informatics
and Mathematics, Keio University).  All Rights Reserved.  This work is
distributed under the W3C® Software License in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


## NLM XML XSLT Files

The NLM XML DTD and preview XSLT are original or modified versions
of the NLM XML DTD tools, released at:

http://dtd.nlm.nih.gov/publishing/
http://dtd.nlm.nih.gov/tools/tools.html

These files are released without any obvious licensing terms.  They
should be considered copyright the National Center for Biotechnology
Information (NCBI), the National Library of Medicine (NLM), and the
developers of the tag set, Mulberry Technologies, Inc.


## BMC XSLT Files

The BMC DTD and HTML preview XSLT are released without an obvious
license.  The public release page is located here:

http://www.biomedcentral.com/about/xml/

The rest of the BMC journal content is released as CC-BY; it is not
obvious whether that license should apply to these files as well.
